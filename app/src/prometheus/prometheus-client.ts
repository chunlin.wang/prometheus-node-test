import { Injectable } from '@nestjs/common';
import * as client from 'prom-client';
import * as process from 'process';

const CONNECTED_FLEET_RECEIVED_FRAMES = 'connected_fleet_received_frames';
const CONNECTED_FLEET_RECEPTION_TO_PROCESSING =
  'connected_fleet_reception_to_processing';
const CONNECTED_FLEET_RECEPTION_TO_PROCESSED =
  'connected_fleet_reception_to_processed';
const CONNECTED_FLEET_AGE = 'connected_fleet_age';
const CONNECTED_FLEET_PROCESS_DURATION = 'connected_fleet_process_duration';
const CONNECTED_FLEET_PROCESS_FULL_DURATION =
  'connected_fleet_process_full_duration';
const CONNECTED_FLEET_PUSH_DURATION = 'connected_fleet_push_duration';
const CONNECTED_FLEET_PUSHING_DURATION = 'connected_fleet_pushing_duration';
const CONNECTED_FLEET_PROCESSED_MESSAGE = 'connected_fleet_processed_messages';
const CONNECTED_FLEET_PUSH_TO_SQS = 'connected_fleet_push_to_sqs';
const CONNECTED_FLEET_PUSH_TO_KINESIS = 'connected_fleet_push_to_kinesis';


@Injectable()
export class PrometheusClient {
  [x: string]: any;

  constructor() {
    this.pushGatewayUrl = 'http://pushgateway:9091';
    this.pushTimeOut = 5000;
    this.gaugeMetrics = [];
    this.histogramMetrics = [];
    this.summaryMetrics = [];
    this.counterMetrics = [];
    const collectDefaultMetrics = client.collectDefaultMetrics;
    this.register = new client.Registry();
    const defaultLabels = {
      project: 'tc-web',
      serviceName: 'fleet-operating-system',
      environment: process.env.ENVIRONMENT,
      hostname: process.env.HOSTNAME,
    };
    this.register.setDefaultLabels(defaultLabels);

    collectDefaultMetrics({
      register: this.register,
      prefix: 'fleet_operating_system_',
    });
  }

  /**
   * Create Histogram Metric if it doesn't exist.
   *
   * @param {string} name
   * @param {string} help
   * @param {object} labelNames
   * @param {number[]} buckets
   * @returns {Promise<void>}
   */
  async createHistogramMetric({
    name,
    help = '',
    labelNames = [],
    buckets = [0.1, 5, 15, 50, 100, 500, 1000],
  }) {
    if (this.histogramMetrics[name] instanceof client.Histogram) {
      return;
    }

    const nameType = `${name}_histogram`;

    this.histogramMetrics[name] =
      (await client.register.getSingleMetric(nameType)) ??
      new client.Histogram({ name: nameType, help, labelNames, buckets });

    await this.register.registerMetric(this.histogramMetrics[name]);
  }

  /**
   * Observer data.
   *
   * @param {string} name
   * @param {object} labels
   * @param {*} value
   * @returns {Promise<void>}
   */
  async histogramMetricObserve(name, labels = {}, value) {
    await this.createHistogramMetric({
      name: name,
      help: name,
      labelNames: ['operatingSystem', 'step', 'processName'],
    });

    await this.histogramMetrics[name].observe(labels, value);
  }

  /**
   * Create Histogram Metric if it doesn't exist.
   *
   * @param {string} name
   * @param {string} help
   * @param {object} labelNames
   * @param {number[]} percentiles
   * @returns {Promise<void>}
   */
  async createSummaryMetric({
    name,
    help = '',
    labelNames = [],
    percentiles = [0.01, 0.1, 0.9, 0.99],
  }) {
    if (this.summaryMetrics[name] instanceof client.Summary) {
      return;
    }

    const nameType = `${name}_summary`;

    this.summaryMetrics[name] =
      (await client.register.getSingleMetric(nameType)) ??
      new client.Summary({ name: nameType, help, labelNames, percentiles });

    await this.register.registerMetric(this.summaryMetrics[name]);
  }

  /**
   * Observer data.
   *
   * @param {string} name
   * @param {object} labels
   * @param {*} value
   * @returns {Promise<void>}
   */
  async summaryMetricObserve(name, labels = {}, value) {
    await this.createSummaryMetric({
      name: name,
      help: name,
      labelNames: ['operatingSystem', 'step', 'processName'],
    });

    await this.summaryMetrics[name].observe(labels, value);
  }

  /**
   * Create Gauge Metric if it doesn't exist.
   *
   * @param {string} name
   * @param {string} help
   * @param {object} labelNames
   * @param {number[]} buckets
   * @returns {Promise<void>}
   */
  async createGaugeMetric({ name, help = '', labelNames = [] }) {
    if (this.gaugeMetrics[name] instanceof client.Gauge) {
      return;
    }

    const nameType = `${name}_gauge`;

    this.gaugeMetrics[name] =
      (await client.register.getSingleMetric(nameType)) ??
      new client.Gauge({ name: nameType, help, labelNames });

    await this.register.registerMetric(this.gaugeMetrics[name]);
  }

  /**
   * Guage metric set data for label.
   *
   * @param {string} name
   * @param {object} labels
   * @param {*} value
   * @returns {Promise<void>}
   */
  async gaugeMetricSetValue(name, labels = {}, value) {
    await this.createGaugeMetric({
      name: name,
      help: name,
      labelNames: ['operatingSystem', 'step', 'processName'],
    });

    await this.gaugeMetrics[name].set(labels, value);
  }

  /**
   * Create Gauge Metric if it doesn't exist.
   *
   * @param {string} name
   * @param {string} help
   * @param {object} labelNames
   * @param {number[]} buckets
   * @returns {Promise<void>}
   */
  async createCounterMetric({ name, help = '', labelNames = [] }) {
    if (this.counterMetrics[name] instanceof client.Counter) {
      return;
    }

    const nameType = `${name}_counter`;

    this.counterMetrics[name] =
      (await client.register.getSingleMetric(nameType)) ??
      new client.Counter({ name: nameType, help, labelNames });

    await this.register.registerMetric(this.counterMetrics[name]);
  }

  /**
   * Guage metric set data for label.
   *
   * @param {string} name
   * @param {object} labels
   * @param {*} value
   * @returns {Promise<void>}
   */
  async counterMetricSetValue(name, labels = {}, value) {
    await this.createCounterMetric({
      name: name,
      help: name,
      labelNames: ['operatingSystem', 'step', 'processName'],
    });

    await this.counterMetrics[name].inc(labels, value);
  }

  /**
   * Observe the value of metric process data.
   *
   * @param {{operatingSystem: string, step: string, processName: string, received: number, processed: number, durations: {age: number, during: number, before: number, after: number}}} stats
   * @returns {Promise<void>}
   */
  async processDataMetricObserve(stats) {
    try {
      if (!this.isActivatedProvider(stats.operatingSystem)) {
        return;
      }

      const metricLabels = {
        operatingSystem: stats.operatingSystem,
        step: stats.step,
        processName: stats.processName,
      };

      await Promise.all([
        this.counterMetricSetValue(
          CONNECTED_FLEET_RECEIVED_FRAMES,
          metricLabels,
          stats.received,
        ),
        this.counterMetricSetValue(
          CONNECTED_FLEET_RECEPTION_TO_PROCESSING,
          metricLabels,
          stats.durations.before,
        ),
        this.counterMetricSetValue(
          CONNECTED_FLEET_RECEPTION_TO_PROCESSED,
          metricLabels,
          stats.durations.after,
        ),
        this.counterMetricSetValue(
          CONNECTED_FLEET_AGE,
          metricLabels,
          stats.durations.age,
        ),
        this.counterMetricSetValue(
          CONNECTED_FLEET_PROCESS_DURATION,
          metricLabels,
          stats.durations.during,
        ),
        this.counterMetricSetValue(
          CONNECTED_FLEET_PROCESSED_MESSAGE,
          metricLabels,
          stats.processed,
        ),
      ]);

      await this.pushAddToPushGateway();
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Observe the value of metric push.
   *
   * @param {{operatingSystem: string, step: string, durations: {full: number, push: number, pushing: number}}} stats
   * @returns {Promise<void>}
   */
  async pushMetricObserve(stats) {
    try {
      if (!this.isActivatedProvider(stats.operatingSystem)) {
        return;
      }

      const metricLabels = {
        operatingSystem: stats.operatingSystem,
        step: stats.step,
      };

      await Promise.all([
        this.counterMetricSetValue(
          CONNECTED_FLEET_PROCESS_FULL_DURATION,
          metricLabels,
          stats.durations.full,
        ),
        this.counterMetricSetValue(
          CONNECTED_FLEET_PUSH_DURATION,
          metricLabels,
          stats.durations.push,
        ),
        this.counterMetricSetValue(
          CONNECTED_FLEET_PUSHING_DURATION,
          metricLabels,
          stats.durations.pushing,
        ),
      ]);

      await this.pushAddToPushGateway();
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Observe the value of metric push to sqs.
   *
   * @param {{operatingSystem: string, step: string, n: number}} stats
   * @returns {Promise<void>}
   */
  async pushToSqsMetricObserve(stats) {
    try {
      if (!this.isActivatedProvider(stats.operatingSystem)) {
        return;
      }

      const metricLabels = {
        operatingSystem: stats.operatingSystem,
        step: stats.step,
      };

      await this.counterMetricSetValue(
        CONNECTED_FLEET_PUSH_TO_SQS + '_counter',
        metricLabels,
        stats.n,
      );
      await this.pushAddToPushGateway();
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Observe the value of metric push to kinesis.
   *
   * @param {{operatingSystem: string, step: string, n: number}} stats
   * @returns {Promise<{
    resp?: unknown;
    body?: unknown;
}>}
   */
  async pushToKinesisMetricObserve(stats): Promise<{
    resp?: unknown;
    body?: unknown;
  }> {
    try {
      if (!this.isActivatedProvider(stats.operatingSystem)) {
        return;
      }

      const metricLabels = {
        operatingSystem: stats.operatingSystem,
        step: stats.step,
      };

      await this.counterMetricSetValue(
        CONNECTED_FLEET_PUSH_TO_KINESIS + '_counter',
        metricLabels,
        stats.n,
      );

      await this.summaryMetricObserve(
        CONNECTED_FLEET_PUSH_TO_KINESIS + '_summary',
        metricLabels,
        stats.n,
      );

      return await this.pushAddToPushGateway();
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Push metrics to push gateway.
   *
   * @param {string} jobName
   * @returns {Promise<*>}
   */
  async pushAddToPushGateway() {
    try {
      const gateway = new client.Pushgateway(
        this.pushGatewayUrl,
        {
          timeout: this.pushTimeOut,
        },
        this.register,
      );

      return gateway.pushAdd({
        jobName: `${process.env.HOSTNAME}-${process.pid}`,
      });
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Get metrics.
   * @returns {Promise<string>}
   */
  async getMetrics() {
    return this.register.metrics();
  }

  /**
   * Check if operatingSystem is activated to have dashboard.
   *
   * @param {string} operatingSystem
   * @returns {boolean}
   */
  isActivatedProvider(operatingSystem: string) {
    return true;

    // const activatedProviders = ['kuantic'];

    // return activatedProviders.includes(operatingSystem);
  }
}
