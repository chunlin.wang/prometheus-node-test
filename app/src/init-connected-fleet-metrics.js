// eslint-disable-next-line @typescript-eslint/no-var-requires
const http = require('node:http');
const prometheusHost = 'http://prometheus:9090';

(async () => {
  const { data } = await (
    await fetch(`${prometheusHost}/api/v1/label/__name__/values`)
  ).json();

  const metrics = data.filter((metric) =>
    metric.startsWith('connected_fleet_'),
  );

  console.log(metrics);

  for (const metric of metrics) {
    await fetch(
      `${prometheusHost}/api/v1/admin/tsdb/delete_series?match[]=${metric}{}`,
      {
        method: 'POST',
      },
    );

    console.log(`${metric} has been deleted.`);
  }
})();
