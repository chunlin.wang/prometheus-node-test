/* eslint-disable @typescript-eslint/no-var-requires */

// const { PrometheusClient } = require('./prometheus/prometheus-client');

import { PrometheusClient } from './prometheus/prometheus-client';
import { delay } from 'bluebird';

// const { delay } = require('bluebird');

(async () => {
  const prometheusClient = new PrometheusClient();

  for (let i = 0; i < 6; ++i) {
    const res1 = await prometheusClient.pushToKinesisMetricObserve({
      operatingSystem: 'kuantic',
      setp: 'push-to-kinesis',
      n: Math.ceil(Math.random() * 10),
    });

    console.log('kuantic', res1.body);

    const res2 = await prometheusClient.pushToKinesisMetricObserve({
      operatingSystem: 'psa',
      setp: 'push-to-kinesis',
      n: Math.ceil(Math.random() * 10),
    });

    console.log('psa', res2.body);

    await delay(5000);
  }
})();
